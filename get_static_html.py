import os
from lib.page_maker import get_html, load_covid_data

page_dir = 'public'
page_name = 'index.html'

covid_data = load_covid_data()
page = get_html(covid_data)

if not os.path.isdir(page_dir):
    os.mkdir(page_dir)
page_path = os.path.join(page_dir, page_name)
open(page_path, 'w', encoding='utf-8').write(page)
print(f'Page saved as "{page_path}"')
