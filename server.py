from http.server import HTTPServer
from lib.http_handler import Handler

if __name__ == '__main__':
    server_address = ('', 8080)
    httpd = HTTPServer(server_address, Handler)
    print("Server Starts - %s:%s" % server_address)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print("Server Stops - %s:%s" % server_address)