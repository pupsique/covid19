from http.server import BaseHTTPRequestHandler
from lib.page_maker import get_html, load_covid_data
from lib.cacher import cache

class Handler(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/':
            self.send_response(200)
            self.send_header('Content-Type', 'text/html')
            self.end_headers()
            try:
                covid_data = cache(load_covid_data)
                response = get_html(covid_data)
                self.wfile.write(response.encode('utf-8'))
            except Exception as e:
                self.send_error(500)
                print(f'{type(e).__name__}: {e}')
        else:
            self.send_error(404)
