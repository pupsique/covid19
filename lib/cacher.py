import pickle, os

file_name = 'covid_data.pickle'  # delete this file to update data on next request

def cache(load_data):
    if os.path.isfile(file_name):
        with open(file_name, 'rb') as f:
            return pickle.load(f)
    else:
        data = load_data()
        with open(file_name, 'wb') as f:
            pickle.dump(data, f)
        return data
