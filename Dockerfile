FROM python:alpine
EXPOSE 8080
WORKDIR /app
COPY . .
CMD [ "python", "-u", "./server.py" ]