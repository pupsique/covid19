Вдохновившись видосом [How To Tell If We're Beating COVID-19](https://youtu.be/54XLXg4fYsc)
от [minutephysics](https://www.youtube.com/channel/UCUHW94eEFW7hkUMVaZz4eDg),
я накидал за пару дней сервис, который на основе данных
с [Карты распространения коронавируса в России и мире](https://yandex.ru/web-maps/covid19)
от [Яндекса](https://yandex.ru) строит графики, аналогичные тем, что на странице
[Covid Trends](https://aatishb.com/covidtrends/)

## [Посмотреть](https://himura.gitlab.io/covid19)