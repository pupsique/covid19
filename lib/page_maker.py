from string import Template
from datetime import datetime, timezone, timedelta
from lib.data_loader import Covid19DataLoader
from lib.data_processor import get_trends, y_axis_window_days
import json
import os

page_path = os.path.join(
                os.path.dirname(
                    os.path.dirname(
                        os.path.realpath(__file__)
                    )
                ),
                'index.html'
            )

def load_covid_data():
    print('Updating Covid-19 data from Yandex...')
    raw_data = Covid19DataLoader().load()
    print('Calculating trends...')
    trends = get_trends(raw_data['items'])
    return {'raw_data': raw_data, 'trends': trends}

def get_data_info(timestamp, subtitle):
    url = Covid19DataLoader.page_url
    info = f'данные получены с <a href="{url}" target=_blank>{url}</a>'
    info += '<br/>и актуальны на '
    update_time = datetime.fromtimestamp(timestamp, tz=timezone(timedelta(hours=3))).strftime('%-d.%m %H:%M')
    info += subtitle.replace('{timestamp}', update_time)
    return info

def get_html(covid_data):
    template_str = open(page_path, 'r', encoding='utf-8').read()
    template = Template(template_str)
    covid_trends_str = json.dumps(covid_data['trends'])
    data_info = get_data_info(covid_data['raw_data']['ts'], covid_data['raw_data']['subtitle'])
    page = template.substitute(
        covid_trends=covid_trends_str, 
        data_info=data_info,
        window=y_axis_window_days
    )
    return page
